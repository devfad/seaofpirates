﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHP : MonoBehaviour {
    public float Health;

    void Start()
    {
        
    }

    public void Damage(float dmg)
    {
        Health-=dmg;
        if (Health < 0)
        {
            Destroy(gameObject);
            GameObject.Find("RespawnShip").GetComponent<RespawnShip>().SpawnShip();
        }
    }
}
