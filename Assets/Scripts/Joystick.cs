﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joystick : MonoBehaviour {
    public GameObject canvas, joystick;

    RectTransform canvasRect, shell, stick, circle;
    bool mouseDown;
    
    public float Distance { get; set; }
    public Vector3 Direction { get; set; }

    void Start () {
        canvasRect = canvas.GetComponent<RectTransform>();
        shell = joystick.GetComponent<RectTransform>();
        stick = joystick.transform.GetChild(0).GetComponent<RectTransform>();
        circle = joystick.transform.GetChild(1).GetComponent<RectTransform>();

        Distance = 0;
        Direction = Vector3.zero;
    }
	
	void Update () {
        if (Input.GetMouseButtonDown(0)) MouseDown();
        if (Input.GetMouseButtonUp(0)) MouseUp();
        if (mouseDown) MouseMove();
    }

    void MouseDown()
    {
        joystick.SetActive(true);
        shell.position = Input.mousePosition;
        stick.position = Input.mousePosition;
        circle.position = Input.mousePosition;
        mouseDown = true;
    }

    void MouseMove()
    {
        Vector3 pos = Input.mousePosition;
        float size = Screen.width / canvasRect.rect.width * circle.rect.width  / 2;

        Direction = Vector3.Normalize(pos - shell.position);

        if (Vector3.Distance(pos, shell.position) > size)
            pos = shell.position + Direction * size;
        
        Distance = Vector3.Distance(pos, shell.position) / size;
        Distance = Distance > 1 ? 1 : Distance;   

        stick.position = pos;
    }

    void MouseUp()
    {
        Distance = 0;
        Direction = Vector3.zero;
        joystick.SetActive(false);
        mouseDown = false;
    }
}
