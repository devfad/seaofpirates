﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour {
    string enemyTag;
    float rangeFire, rateFire, bulletSpeed, minDistance;
    GameObject bullet;

    float _rateFire;
    Parabola parabola;

    public void SetParams(GameObject bullet, string enemyTag, float rangeFire, float rateFire, float bulletSpeed, float minDistance)
    {
        this.bullet = bullet;
        this.enemyTag = enemyTag;
        this.rangeFire = rangeFire;
        this.rateFire = rateFire;
        this.bulletSpeed = bulletSpeed;
        this.minDistance = minDistance;
    }

    void Start()
    {
        parabola = new Parabola();
    }

    void Update()
    {
        if (_rateFire > 0) _rateFire--;

        LaunchRay rey = null;
        GameObject nearObj = null;
        float curentMinDistance = minDistance;

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag(enemyTag))
        {
            float distance = Vector3.Distance(obj.transform.position, transform.position);
            if (distance < curentMinDistance)
            {
                nearObj = obj;
                curentMinDistance = distance;
            }
        }

        if (nearObj != null)
        {
            Vector3 direction = (nearObj.transform.position-transform.position).normalized;
            if (Vector3.Distance(direction, transform.forward) > 1f) return;

            rey = new LaunchRay(transform.position, direction, rangeFire, Color.red);

            if (rey.Isset) Fire(rey.Transform.GetChild(0), rey.Transform.tag, rey.Distance);            
        }
    }

    void Fire(Transform target, string currentTag, float distance)
    {
        if (_rateFire > 0 || currentTag != enemyTag) return;

        Vector3 startPos = transform.position;
        Vector3 endPos = target.position;

        GameObject obj = Instantiate(bullet, startPos, transform.rotation);
        Physics.IgnoreCollision(obj.GetComponent<Collider>(), transform.parent.parent.GetComponent<Collider>());

        parabola.Plot(startPos, endPos, bulletSpeed, Physics.gravity);

        if (parabola.GetStartVelocity() != Vector3.zero)
            obj.GetComponent<Rigidbody>().AddForce(parabola.GetStartVelocity(), ForceMode.Impulse);

        _rateFire = rateFire;
    }
}
