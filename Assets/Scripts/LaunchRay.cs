﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaunchRay {
    Transform obj = null;
    RaycastHit hit = new RaycastHit();

    public bool Isset { get { return obj != null; } }
    public Transform Transform { get { return obj; }  }
    public float Distance { get { return hit.distance; } }

    public Vector3 RayDirection { get; set; }

    public LaunchRay(Vector3 position, Vector3 direction, float distance, Color color)
    {
        Debug.DrawRay(position, direction * distance, color);

        RayDirection = direction;

        Ray ray = new Ray(position, direction);
        if (Physics.Raycast(ray, out hit, distance))
            obj = hit.transform;
    }
}
