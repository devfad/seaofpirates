﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletColision : MonoBehaviour {
    void Update()
    {
        if(transform.position.y < 0f)
            Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<ShipHP>())
            other.GetComponent<ShipHP>().Damage(1);

        Destroy(gameObject);
    }
}
