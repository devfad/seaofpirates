﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    public Joystick joystick;

    void Update()
    {
        Vector3 direction = new Vector3(joystick.Direction.x, 0f, joystick.Direction.y);
        if (joystick.Distance < 0.1f) direction = Vector3.zero;
        GetComponent<ShipMove>().AddForce(direction, joystick.Distance);
    }
}
