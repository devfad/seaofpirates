﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RespawnShip : MonoBehaviour {
    public float Distance;
    public GameObject EnemyObj;

	void Start () {
		
	}
	
	void Update () {
		
	}

    public void SpawnShip()
    {
        Vector3 pos = Vector3.zero;

        do {
            pos = new Vector3(Random.Range(0, Distance * 2) - Distance, EnemyObj.transform.position.y, Random.Range(0, Distance * 2) - Distance);
        } while (!CheckColision(pos));

        Instantiate(EnemyObj, pos, Quaternion.Euler(0, Random.Range(0, 360), 0));
    }

    bool CheckColision(Vector3 pos)
    {
        LaunchRay rey = new LaunchRay(pos + (Vector3.up * 5), Vector3.down, 100, Color.black);
        if (rey.Isset) return false;
        return true;
    }
}
