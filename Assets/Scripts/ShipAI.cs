﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipAI : MonoBehaviour {
    public float DistanceDetect;
    
    Vector3 direction;

    void Start()
    {
        NewDirection();
    }

    void Update()
    {
        LaunchRay launchRay = new LaunchRay(transform.position, direction, DistanceDetect, Color.green);

        if(launchRay.Isset) NewDirection();
        else if (Random.Range(0f, 100f) < 0.4f) NewDirection();

        GetComponent<ShipMove>().AddForce(direction, 1f);
    }

    void NewDirection()
    {
        float angle = Random.Range(0f,360f)/180f*Mathf.PI;
        direction = new Vector3(Mathf.Cos(angle), 0f, Mathf.Sin(angle));
    }

    void OnCollisionEnter(Collision collision)
    {
        NewDirection();
    }
}
