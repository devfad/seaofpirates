﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public GameObject obj;

    Vector3 margin;
    
	void Start () {
        margin = transform.position - obj.transform.position;
    }
	
	void Update () {
        transform.position = obj.transform.position + margin;
	}
}
