﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipFire : MonoBehaviour {
    public string EnemyTag;
    public float RangeFire, RateFire, BulletSpeed, MinDistance;
    public GameObject bullet;
    
    void Start () {
        foreach(var obj in transform.GetChild(0).GetComponentsInChildren<Cannon>())
            obj.SetParams(bullet, EnemyTag, RangeFire, RateFire, BulletSpeed, MinDistance);
    }
	
	void Update () {
    }
}
