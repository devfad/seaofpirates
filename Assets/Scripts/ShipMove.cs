﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMove : MonoBehaviour {
    public float Speed;
    public float SpeedDirection;

    Rigidbody rb;
    Vector3 direction;
    float force;

    void Start()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;

        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if (direction == Vector3.zero) return;;

        Vector3 velocity = transform.forward * Speed * force;
        if (Vector3.Magnitude(rb.velocity) < Vector3.Magnitude(velocity))
            rb.velocity = velocity;

        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(direction), SpeedDirection);
    }

    public void AddForce(Vector3 direction, float force)
    {
        this.direction = direction;
        this.force = force;
    }
}
